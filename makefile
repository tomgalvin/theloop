# Load the correct makefile to run based on the SYSTEM variable. This variable
# can either be WINDOWS or LINUX. The default is value is WINDOWS.
#
# On Linux, use the command 'export SYSTEM=LINUX' to avoid needing to set SYSTEM
# on every invocation to make.

# Set the default value
ifndef SYSTEM
	SYSTEM	:=	WINDOWS
endif

# Include the Windows makefile
ifeq ($(SYSTEM),WINDOWS)
	include makefile.windows
endif

# Include the Linux makefile
ifeq ($(SYSTEM),LINUX)
	include makefile.linux
endif
