#include "cubie.h"
#include "game_over.h"
#include <set>

Cubie::Cubie(const Coordinate& pos)
{
	mPosition = pos;
}



void Cubie::move(const Direction direction)
{
	Coordinate newPos = getCoordinate(mPosition, direction);
	// Check if the cubie is colliding with the wall
	if( newPos.x < 0 ||
		newPos.x >= MAP_WIDTH ||
		newPos.y < 0 ||
		newPos.y >= MAP_HEIGHT){
		throw(game_over(CubieState(mPosition, mDirection)));	
	}

	mPosition = newPos;
}
