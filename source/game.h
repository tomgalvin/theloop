#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include "cubie_manager.h"
#include "game_over.h"

class Game
{
	sf::Time tickTime; // The amount of time between each tick
	sf::Time waitTime; // The amount of time to wait in the game over scene
	sf::Time elapsedTime; // The amount of time elpased since the last tick
	
	sf::RenderWindow window;
	sf::RenderTexture worldTex;

	GameOverScene gameOverScene;

	CubieManager cubieManager;

	enum { Intro, Playing, GameOver } state;

	void newGame();
	void update(sf::Time delta);
	void handleEvents();
	void handleKeyEvent(const sf::Event::KeyEvent& event);
	void draw();
public:
	Game();

	void run();
};

#endif // GAME_H