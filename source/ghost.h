#ifndef GHOST_H
#define GHOST_H

#include <queue>
#include <SFML/Graphics.hpp>
#include "coordinate.h"
#include "cubie.h"

// Ghost is a cubie that must follow a certain path and kill itself once it's done
class Ghost : public Cubie
{
private:
	int historyIndex;
public:
	Ghost(const Coordinate& pos);

	void tick();

	// Return the history index
	int getHistoryIndex();

	// Allows the manager to set the coordinate
	void setPosition(const Coordinate& pos);
	
	// Draw the ghost onto a render target
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif // GHOST_H
