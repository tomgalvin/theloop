#ifndef CUBIE_H
#define CUBIE_H

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <exception>
#include "coordinate.h"

class Cubie : public sf::NonCopyable, public sf::Drawable
{
protected:
	Coordinate mPosition; // The Cubie's position in the game world
	Direction mDirection; // The direction the cubie would move in the next tick

public:
	Cubie(const Coordinate& pos);

	inline Coordinate getPosition() { return mPosition; }
	inline Direction getDirection() { return mDirection; }
	inline void setDirection(const Direction dir) { mDirection = dir; }

	// Move the cubie one tile in the given direction
	void move(const Direction direction);

	virtual void tick() = 0;
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;
};

#endif // CUBIE_H
