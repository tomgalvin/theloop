#include "game_over.h"
#include "resources.h"

game_over::game_over(const CubieState& state1,  const CubieState& state2) : 
	std::exception(),
	type(GHOST), 
	obj1(state1), 
	obj2(state2) { Resources::getSound("collision").play(); }

game_over::game_over(const CubieState& state) :
	std::exception(),
	type(WALL),
	obj1(state),
	obj2(CubieState(Coordinate(), Direction::EAST)) { Resources::getSound("collision").play(); } // Some dummie values since object will not be used

game_over::game_over() :
	std::exception(),
	type(WEAKNESS),
	obj1(CubieState(Coordinate(), Direction::EAST)), // Some dummie values since object will not be used
	obj2(CubieState(Coordinate(), Direction::EAST)) {} // Some dummie values since object will not be used


void GameOverScene::init(game_over args)
{
	scene.create(MAP_WIDTH * TILE_SIZE, MAP_HEIGHT * TILE_SIZE);
	scene.clear(sf::Color::Transparent);

	switch(args.type){
	case GHOST:
		renderCollision(args.obj1);
		renderCollision(args.obj2);
		break;
	case WALL:
		renderCollision(args.obj1);
		break;
	}

	scene.display();
}

void GameOverScene::renderCollision(const CubieState& obj)
{
	sf::Sprite sprite(Resources::getTexture("collision"));
	sprite.setPosition(obj.coordinate.x * TILE_SIZE + (sprite.getGlobalBounds().width / 2.0f), obj.coordinate.y * TILE_SIZE + (sprite.getGlobalBounds().height / 2.0f));
	
	// center the sprite for the rotation
	sprite.setOrigin(sprite.getGlobalBounds().width / 2.0f, sprite.getGlobalBounds().height / 2.0f);
	
	// Set the proper rotation
	float angle = 0.0f;

	switch(obj.direction){
	case Direction::NORTH:
		angle = 0.0f;
		break;
	case Direction::EAST:
		angle = 90.0f;
		break;
	case Direction::SOUTH:
		angle = 180.0f;
		break;
	case Direction::WEST:
		angle = 270.0f;
		break;
	}
	sprite.setRotation(angle);

	scene.draw(sprite);
}

void GameOverScene::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	sf::Sprite s(scene.getTexture());
	target.draw(s);
}
