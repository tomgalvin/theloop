#include "resources.h"

std::map<sf::String, sf::Texture> Resources::mTextures = std::map<sf::String, sf::Texture>();
std::map<sf::String, sf::Sound> Resources::mSounds = std::map<sf::String, sf::Sound>();
std::map<sf::String, sf::Font> Resources::mFonts = std::map<sf::String, sf::Font>();

sf::SoundBuffer* Resources::collision;
sf::SoundBuffer* Resources::loop;


void Resources::load()
{
	mTextures["player"].loadFromFile("assets/player.png");
	mTextures["ghost"].loadFromFile("assets/ghost.png");
	mTextures["mushroom"].loadFromFile("assets/mushroom.png");
	mTextures["collision"].loadFromFile("assets/collision.png");
	mTextures["go_overlay"].loadFromFile("assets/go_overlay.png");
	mTextures["intro"].loadFromFile("assets/intro.png");

	mFonts["mono"].loadFromFile("assets/mono.ttf");

	collision = new sf::SoundBuffer();
	collision->loadFromFile("assets/collision.wav");

	loop = new sf::SoundBuffer();
	loop->loadFromFile("assets/loop.wav");

	mSounds["collision"].setBuffer(*collision);
	mSounds["loop"].setBuffer(*loop);
}

void Resources::unload()
{
	mTextures.clear();
	mSounds.clear();
	mFonts.clear();

	delete collision;
	delete loop;
}

const sf::Texture& Resources::getTexture(const sf::String& name)
{
	return mTextures.at(name);
}

const sf::Font& Resources::getFont()
{
	return mFonts.at("mono");
}

sf::Sound& Resources::getSound(const sf::String& name)
{
	return mSounds.at(name);
}
