#include "game.h"

Game::Game() : cubieManager(20)
{
	tickTime = sf::seconds(0.35f);
	waitTime = sf::seconds(3.0f);
	window.create(sf::VideoMode((MAP_WIDTH + 1) * TILE_SIZE, (MAP_HEIGHT + 2) * TILE_SIZE), "The Loop", sf::Style::Titlebar | sf::Style::Close);
	worldTex.create(MAP_WIDTH * TILE_SIZE, MAP_HEIGHT * TILE_SIZE);
	state = Intro;
}

void Game::newGame()
{
	cubieManager.restart();
	elapsedTime = sf::Time::Zero;
	state = Playing;
}

void Game::run()
{
	
	sf::Clock clock;
	while(window.isOpen()){
		update(clock.restart());
		handleEvents();
		draw();
	}
}

void Game::update(sf::Time delta)
{
	elapsedTime += delta;
	switch(state){
	case Playing:
		if(elapsedTime >= tickTime){
			try{
				cubieManager.tick();
			}
			catch(game_over args){
				gameOverScene.init(args);
				state = GameOver;
			}
			elapsedTime = sf::Time::Zero;
		}
		break;
	}
}

void Game::draw()
{
	window.clear(sf::Color(0, 64, 128));
	
	switch(state) {
	case Intro:{
		sf::Sprite sprite (Resources::getTexture("intro"));
		window.draw(sprite);
		break;
	}
	case Playing: case GameOver:

		// Draw the game world
		worldTex.clear(sf::Color(0, 128, 255));
		cubieManager.draw(worldTex);

		// Draw the game over scene
		if(state == GameOver){
			worldTex.draw(gameOverScene);
		}

		worldTex.display();
		sf::Sprite world(worldTex.getTexture());
		world.setPosition(TILE_SIZE / 2, TILE_SIZE / 2);
		window.draw(world);

		sf::Text txt;
		txt.setFont(Resources::getFont());
		// Draw the score text
		txt.setString(cubieManager.getScoreString());
		txt.setColor(sf::Color::White);
		txt.setPosition(TILE_SIZE / 2, TILE_SIZE * MAP_HEIGHT + (TILE_SIZE / 2));
		window.draw(txt);

		// Draw the energy text
		txt.setString(cubieManager.getEnergyString());
		if(cubieManager.getEnergy() < 9)
			txt.setColor(sf::Color::Red);
		else if(cubieManager.getEnergy() > 36)
			txt.setColor(sf::Color::Green);
		else
			txt.setColor(sf::Color::White);
		txt.setPosition(window.getSize().x - txt.getLocalBounds().width - (TILE_SIZE / 2), TILE_SIZE * MAP_HEIGHT + (TILE_SIZE / 2));
		window.draw(txt);

		// Draw the game over overlay
		if(state == GameOver){	
			sf::Sprite go_overlay(Resources::getTexture("go_overlay"));
			window.draw(go_overlay);
		}

	break;
	}
	window.display();
}

void Game::handleEvents()
{
	sf::Event event;
	while(window.pollEvent(event)){
		switch(event.type){
		case sf::Event::Closed:
			window.close();
			break;
		case sf::Event::KeyPressed:
			handleKeyEvent(event.key);
			break;
		}
	}
}

void Game::handleKeyEvent(const sf::Event::KeyEvent& event){
	// Handle the following events based on the current state
	switch(state){
	case Intro: case GameOver:
		switch(event.code){
		case sf::Keyboard::Escape:
			window.close();
			break;
		case sf::Keyboard::Return:
			newGame();
			break;
		}
		break;
	case Playing:
		switch(event.code){
		case sf::Keyboard::Escape:
			window.close();
			break;
		case sf::Keyboard::W: case sf::Keyboard::Up:
			cubieManager.setPlayerDirection(Direction::NORTH);
			break;
		case sf::Keyboard::A: case sf::Keyboard::Left:
			cubieManager.setPlayerDirection(Direction::WEST);
			break;
		case sf::Keyboard::S: case sf::Keyboard::Down:
			cubieManager.setPlayerDirection(Direction::SOUTH);
			break;
		case sf::Keyboard::D: case sf::Keyboard::Right:
			cubieManager.setPlayerDirection(Direction::EAST);
			break;
		}
		break;
	}
}
