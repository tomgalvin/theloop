#ifndef GAME_OVER_H
#define GAME_OVER_H

#include "coordinate.h"
#include <SFML/Graphics.hpp>
#include <exception>

// The varius ways a game over can occur
enum GameOverType {
	GHOST,		// There was some collision with a ghost (either player with ghost or ghost with ghost)
	WALL,		// The player bumped into a wall
	WEAKNESS	// The player ran out of energy
};

// A need way to handle game-overs would be to implement them like exceptions
class game_over : public std::exception
{
public:
	const GameOverType type;
	const CubieState obj1; // For GHOST and WALL
	const CubieState obj2; // For GHOST

	game_over(const CubieState& state1,  const CubieState& state2); // Constructor for GHOST
	game_over(const CubieState& state);	 // Constructor for WALL
	game_over(); // Constructor for WEAKNESS

};

// Manages the drawing of all the game over stuff
class GameOverScene : public sf::Drawable
{
	sf::RenderTexture scene;

	// Render a collision onto the scene based on the given cubie states
	void renderCollision(const CubieState& obj);
public:
	void init(game_over args); // Should be called by whoever catchs the exception
	
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif // GAME_OVER_H
