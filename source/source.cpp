#include <SFML/Graphics.hpp>

#include "resources.h"
#include "game.h"

int main(int argc, char** argv)
{
	Resources::load();
	Game game;

	game.run();

	Resources::unload();

	return 0;
}
