#include "coordinate.h"

bool areAdjacent(const Coordinate& first, const Coordinate& second)
{
	if( getCoordinate(first, Direction::EAST) == second ||
		getCoordinate(first, Direction::WEST) == second ||
		getCoordinate(first, Direction::NORTH) == second ||
		getCoordinate(first, Direction::SOUTH) == second)
		return true;
	else
		return false;
}

Coordinate getCoordinate(const Coordinate& coord, const Direction dir)
{
	Coordinate newCoord = coord;
	switch(dir){
	case Direction::EAST :
		newCoord.x++;
		break;
	case Direction::WEST :
		newCoord.x--;
		break;
	case Direction::NORTH :
		newCoord.y--;
		break;
	case Direction::SOUTH :
		newCoord.y++;
	}

	return newCoord;
}

Direction findDirection(const Coordinate& origin, const Coordinate& target)
{
	// Make sure the two coordinates are adjacent
	if(!areAdjacent(origin, target))
		throw std::invalid_argument("The two given coordinates aren't adjacent");

	if(origin.x < target.x)
		return Direction::EAST;
	else if(origin.x > target.x)
		return Direction::WEST;
	else if(origin.y > target.y)
		return Direction::NORTH;
	else
		return Direction::SOUTH;
}
